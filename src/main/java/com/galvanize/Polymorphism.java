package com.galvanize;

public class Polymorphism {
    public static void main(String[] args) {

        Animal myAnimal = new Animal();
        myAnimal.eat();
//        myAnimal.sound();

        Cat myCat = new Cat();
        myCat.eat(3);
        myCat.eat();
//        myCat.sound();

//        GuineaPig myGuineaPig = new GuineaPig();
//        myGuineaPig.eat();
//        myGuineaPig.sound();

    }
}
