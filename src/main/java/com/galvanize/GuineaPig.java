package com.galvanize;

public class GuineaPig extends Animal {

    public void eat() {
        System.out.println("chew chew");
    }

    public void sound() {
        System.out.println("squeak squeak");
    }
}