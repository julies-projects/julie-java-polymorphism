package com.galvanize;

public class Cat extends Animal {
    private int breed;

    public void eat() {
        System.out.println("crunch crunch");
    }

    public void eat(int numberOfTimes) {
        for (int i = 0; i < numberOfTimes; i++) {
            System.out.println("crunch crunch");
        }
    }
    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }

}


